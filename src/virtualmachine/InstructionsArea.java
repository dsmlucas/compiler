package virtualmachine;

public class InstructionsArea {
	public Tipos AI[]= new Tipos[1000];
	public int LC;
	
    /**
   * Construtor sem parâmetros.
   * Todos os atributos são inicializados com valores padrões.
   */
  	public InstructionsArea(){
		for(int i=0; i<1000; i++){
			AI[i]=new Tipos();
		}
	}


	public int getLC() {
		return this.LC + 1;
	}

	public void setLC(int LC) {
		this.LC = LC;
	}
	
}