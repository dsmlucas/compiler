package controller;

public class SemanticError extends Exception {
	
	private static final long serialVersionUID = 1L;
    String error;
	
	public SemanticError(String msg) {
		this.error = "ERRO SEMÂNTICO: " +  msg;
	}
	
	public String getMessage(){
		return error;
	}
}
 