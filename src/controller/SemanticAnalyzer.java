package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import bean.SymbolTable;
import model.Symbol;
import table.enumered.Category;
import virtualmachine.*;

public class SemanticAnalyzer {

    public static ArrayList<Instruction> intermediateCode;
    public static ArrayList<Integer> stackIf;
    public static ArrayList<Integer> stackWhile;
    public static ArrayList<Integer> stackRepeat;
    public static ArrayList<Integer> stackCase;
    public static ArrayList<Integer> stackFor;
    public static ArrayList<Integer> stackParameters;
    public static ArrayList<Integer> stackProcedure;
    public static ArrayList<Symbol> stackSymbol;
    public static ArrayList<Symbol> stackSymbolAux;
    public static HashMap<Integer, String> literalArea;

    public static SymbolTable symbolTable;
    public static Integer tableSize;
    public static Symbol constant;
    public static Symbol procedure;
    public static Symbol symbolTmp;

    public static Hipotetica virtualMachine;
    public static InstructionsArea ia;
    public static LiteralsArea la;
    public static Integer currentLevel;
    public static Integer level;
    public static Integer nextFreePosition;
    public static Integer variables;
    public static Integer parameters;
    public static Integer displacement;
    public static Integer levelVariable, displacementVariable;
    public static Integer indexLiteralArea;
    public static Integer indexSymbolTable;
    public static Integer stackAux;
    public static Instruction stackDetour;
    public static Boolean containsParameters;
    public static Boolean isProcedure = false;
    public static String identifierType;
    public static String context;
    public static String callAux;

    public SemanticAnalyzer() {
        intermediateCode = new ArrayList<Instruction>();
        tableSize = 8;
    }

    public void setCall(String call) {
        callAux = call;
    }

    public void executeAction(Integer idCode, Token token) throws SemanticError {

        switch (idCode) {

        case 100:

            initializeStack();

            symbolTable = new SymbolTable(tableSize);

            virtualMachine = new Hipotetica();
            ia = new InstructionsArea();
            la = new LiteralsArea();
            Hipotetica.InicializaAI(ia);
            Hipotetica.InicializaAL(la);

            initializeVariables();
            break;

        case 101:

            intermediateCode.add(new Instruction(ia.LC, "PARA", "-", "-"));
            virtualMachine.IncluirAI(ia, 26, 0, 0);
            break;

        case 102:

            displacement = 3;

            intermediateCode.add(new Instruction(ia.LC, "AMEM", "-", (displacement + variables) + ""));
            virtualMachine.IncluirAI(ia, 24, 0, (displacement + variables));
            break;

        case 104:

            if (Category.VAR.getLabel().equals(identifierType)) {

                symbolTmp = symbolTable.search(token.getName(), currentLevel);

                if (isValidSymbol(symbolTmp)) {
                    throw new SemanticError(getMsgError104(token));
                } else {
                    symbolTable.insert(new Symbol(token.getName(), Category.VAR, currentLevel, displacement, 0));

                    stackSymbol.add(new Symbol(token.getName(), Category.VAR, currentLevel, displacement, 0));
                    stackSymbolAux.add(new Symbol(token.getName(), Category.VAR, currentLevel, displacement, 0));

                    displacement++;
                    variables++;
                }
 
            } else if (Category.PARAMETER.getLabel().equals(identifierType)) {

                symbolTmp = symbolTable.search(token.getName(), currentLevel);

                if (isValidSymbol(symbolTmp)) {
                    throw new SemanticError(getMsgError104(token));
                } else {
                    Symbol domain = new Symbol(token.getName(), Category.PARAMETER, currentLevel, 0, 0);
                    symbolTable.insert(domain);

                    stackSymbol.add(domain);
                    stackSymbolAux.add(domain);
                    parameters++;
                }
            }
            break;

        case 105:

            symbolTmp = symbolTable.search(token.getName(), currentLevel);

            if (isValidSymbol(symbolTmp)) {
                throw new SemanticError(getMsgError104(token));
            } else {
                constant = new Symbol(token.getName(), Category.CONSTANT, currentLevel, 0, 0);
                symbolTable.insert(constant);
                stackSymbolAux.add(constant);
            }
            break;

        case 106:

            constant.setGeneralA(token.getValue());
            break;

        case 107:

            identifierType = Category.VAR.getLabel();
            break;

        case 108:

            procedure = new Symbol(token.getName(), Category.PROCEDURE, currentLevel, ia.LC + 1, 0);
            symbolTable.insert(procedure);
            stackSymbolAux.add(procedure);

            parameters = 0;
            containsParameters = false;

            currentLevel++;

            displacement = 3;
            variables = 0;

            break;

        case 109:

            stackProcedure.add(ia.LC);

            if (containsParameters) {

                procedure.setGeneralB(parameters);

                for (int i = 0; i < parameters; i++) {
                    Symbol p = returnSymbol(stackSymbol);
                    p.setGeneralA(-(parameters - i));
                }
            }

            intermediateCode.add(new Instruction(ia.LC, "DSVS", "-", "?"));
            virtualMachine.IncluirAI(ia, 19, 0, 0);
            stackParameters.add(ia.LC - 1);
            stackParameters.add(parameters);

            parameters = 0;

            break;

        case 110:

            Integer p = returnStack(stackParameters);

            intermediateCode.add(new Instruction(ia.LC, "RETU", "-", String.valueOf(p)));
            virtualMachine.IncluirAI(ia, 1, 0, p);

            this.removeSymbol();

            currentLevel--;

            break;

        case 111:

            identifierType = Category.PARAMETER.getLabel();
            containsParameters = true;

            break;

        case 114:

            symbolTmp = symbolTable.search(token.getName(), currentLevel);

            if (isValidSymbol(symbolTmp)) {

                if (!symbolTmp.getCategory().getLabel().equals(Category.VAR.getLabel())) {

                    throw new SemanticError("Não é uma variável.");

                } else {
                    levelVariable = currentLevel - symbolTmp.getLevel();
                    displacementVariable = symbolTmp.getGeneralA();
                }

            } else {
                throw new SemanticError("Identificador \"" + token.getName() + "\" não declarado.");
            }

            break;

        case 115:

            intermediateCode.add(new Instruction(ia.LC, "ARMZ", String.valueOf(levelVariable), String.valueOf(displacementVariable)));
            virtualMachine.IncluirAI(ia, 4, levelVariable, displacementVariable);

            break;

        case 116:

            symbolTmp = symbolTable.search(token.getName());

            if (isValidSymbol(symbolTmp)) {

                Category c = symbolTable.getTable().get(indexSymbolTable).getCategory();

                if (c.equals(Category.PROCEDURE)) {

                    stackAux = returnStack(stackProcedure);
                    stackDetour = intermediateCode.get(stackAux - 1);
                    stackDetour.setOperator2(String.valueOf(ia.LC));
        
                    intermediateCode.set(stackAux - 1, stackDetour);
                    virtualMachine.AlterarAI(ia, 21, 0, stackDetour.getOp2());

                } else {
                    throw new SemanticError("Não é uma procedure.");
                }

            } else {
                throw new SemanticError("Procedure não declarada.");
            }

            parameters = 0;

            break;

        case 117:

            symbolTmp = symbolTable.search(callAux);

            if (symbolTmp.getGeneralB() != parameters)
                throw new SemanticError("Número de parâmetros inválido.");

            intermediateCode.add(new Instruction(ia.LC, "CALL", String.valueOf(currentLevel), String.valueOf(displacementVariable)));
            virtualMachine.IncluirAI(ia, 4, levelVariable, displacementVariable);

            break;

        case 118:

            parameters++;

            break;

        case 120:

            stackIf.add(ia.LC);
            intermediateCode.add(new Instruction(ia.LC, "DVSF", "-", "?"));
            virtualMachine.IncluirAI(ia, 20, -1, -1);

            break;

        case 121:

            stackAux = returnStack(stackIf);
            stackDetour = intermediateCode.get(stackAux - 1);
            stackDetour.setOperator2(String.valueOf(ia.LC));

            intermediateCode.set(stackAux - 1, stackDetour);
            virtualMachine.AlterarAI(ia, 20, 0, stackDetour.getOp2());

            break;

        case 122:

            stackAux = returnStack(stackIf);
            stackDetour = intermediateCode.get(stackAux - 1);
            stackDetour.setOperator2(String.valueOf(ia.LC + 1));

            intermediateCode.set(stackAux - 1, stackDetour);
            virtualMachine.AlterarAI(ia, 20, 0, stackDetour.getOp2());

            stackIf.add(ia.LC);
            intermediateCode.add(new Instruction(ia.LC, "DSVS", "-", "?"));
            virtualMachine.IncluirAI(ia, 19, 0, 0);

            break;

        case 123:

            stackWhile.add(ia.LC);
            break;

        case 124:

            intermediateCode.add(new Instruction(ia.LC, "DVSF", "-", "?"));
            stackWhile.add(ia.LC);
            virtualMachine.IncluirAI(ia, 20, 0, 0);

            break;

        case 125:

            stackAux = returnStack(stackWhile);
            virtualMachine.AlterarAI(ia, stackAux, 0, ia.LC + 1);

            stackDetour = intermediateCode.get(stackAux - 1);
            stackDetour.setOperator2(String.valueOf(ia.LC + 1));

            intermediateCode.set(stackAux - 1, stackDetour);

            stackAux = returnStack(stackWhile);
            intermediateCode.add(new Instruction(ia.LC, "DSVS", "-", String.valueOf(stackAux)));
            virtualMachine.IncluirAI(ia, 19, 0, stackAux);

            break;

        case 126:

            stackRepeat.add(ia.LC);
            break;

        case 127:

            stackAux = returnStack(stackRepeat);

            intermediateCode.add(new Instruction(ia.LC, "DVSF", "-", String.valueOf(stackAux)));
            virtualMachine.IncluirAI(ia, 20, 0, stackAux);

            break;

        case 128:

            context = "readln";
            break;

        case 129:

            symbolTmp = symbolTable.search(token.getName());

            if (isValidSymbol(symbolTmp)) {

                if (context.equals("readln")) {

                    if (symbolTable.getTable().get(indexSymbolTable).getCategory().equals(Category.VAR)) {

                        intermediateCode.add(new Instruction(ia.LC, "LEIT", "-", "-"));
                        virtualMachine.IncluirAI(ia, 21, 0, 0);

                        Integer localLevel = symbolTable.getTable().get(indexSymbolTable).getLevel() - currentLevel;
                        Integer localDisplacement = symbolTable.getTable().get(indexSymbolTable).getGeneralA();

                        intermediateCode.add(new Instruction(ia.LC, "ARMZ", String.valueOf(localLevel), String.valueOf(localDisplacement)));
                        virtualMachine.IncluirAI(ia, 4, localLevel, localDisplacement);

                    } else {
                        throw new SemanticError("Não é uma variável.");
                    }

                } else if (context.equals("EXPRESSAO")) {

                    Category c = symbolTable.getTable().get(indexSymbolTable).getCategory();

                    if (c.equals(Category.PROCEDURE)) {

                        throw new SemanticError("Não é possível ler uma procedure!");

                    } else if (c.equals(Category.CONSTANT)) {

                        Integer i = symbolTable.getTable().get(indexSymbolTable).getGeneralA();

                        intermediateCode.add(new Instruction(ia.LC, "CRCT", "-", String.valueOf(i)));
                        virtualMachine.IncluirAI(ia, 3, 0, i);

                    } else {

                        Integer levelDifference = currentLevel
                                - symbolTable.getTable().get(indexSymbolTable).getLevel();
                        Integer localDisplacement = symbolTable.getTable().get(indexSymbolTable).getGeneralA();

                        intermediateCode.add(new Instruction(ia.LC, "CRVL", String.valueOf(levelDifference),
                                String.valueOf(localDisplacement)));
                        virtualMachine.IncluirAI(ia, 2, levelDifference, localDisplacement);

                    }
                }
            }

            break;

        case 130:

            intermediateCode.add(new Instruction(ia.LC, "IMPL", "-", String.valueOf(la.LIT)));
            virtualMachine.IncluirAL(la, token.getName());
            virtualMachine.IncluirAI(ia, 23, 0, la.LIT - 1);

            literalArea.put(la.LIT, token.getName());

            break;

        case 131:

            intermediateCode.add(new Instruction(ia.LC, "IMPR", "-", "-"));
            virtualMachine.IncluirAI(ia, 22, 0, 0);

            break;

        case 133:
            while (!stackCase.isEmpty()) {
                // TODO VEIFICAR
                stackAux = returnStack(stackCase);
                String op2 = String.valueOf(ia.LC + 1);
                intermediateCode.get(stackAux).setOperator2(op2);
            }
            intermediateCode.add(new Instruction(ia.LC, "AMEM", "-", "-1"));
            virtualMachine.IncluirAI(ia, 24, 0, -1);

            break;

        case 134:
            intermediateCode.add(new Instruction(ia.LC, "COPI", "-", "-"));
            virtualMachine.IncluirAI(ia, 28, 0, 0);

            intermediateCode.add(new Instruction(ia.LC, "CRCT", "-", token.getName()));// TODO VERIFICAR OP2
            virtualMachine.IncluirAI(ia, 3, 0, 1);

            intermediateCode.add(new Instruction(ia.LC, "CMIG", "-", "-"));
            virtualMachine.IncluirAI(ia, 15, 0, 0);

            while (!stackCase.isEmpty()) {
                stackAux = returnStack(stackCase);
                if (intermediateCode.get(stackAux).getName().equals("DSVS"))
                    break;

                String op2 = String.valueOf(ia.LC + 1);
                intermediateCode.get(stackAux).setOperator2(op2);
            }

            stackCase.add(ia.LC);
            intermediateCode.add(new Instruction(ia.LC, "DVSF", "-", "?"));
            virtualMachine.IncluirAI(ia, 20, 0, 0);

            break;

        case 135:

            intermediateCode.add(new Instruction(ia.LC, "DSVS", "-", "?"));
            virtualMachine.IncluirAI(ia, 19, 0, 0);

            stackAux = returnStack(stackCase);
            String op2 = String.valueOf(ia.LC + 1);
            intermediateCode.get(stackAux).setOperator2(op2);

            stackCase.add(ia.LC - 1); // TODO VERIFICAR

            break;

        case 136:

            intermediateCode.add(new Instruction(ia.LC, "COPI", "-", "-"));
            virtualMachine.IncluirAI(ia, 28, 0, 0);

            intermediateCode.add(new Instruction(ia.LC, "CRCT", "-", token.getName())); // TODO VERIFICAR OP2
            virtualMachine.IncluirAI(ia, 3, 0, 1);

            intermediateCode.add(new Instruction(ia.LC, "CMIG", "-", "-"));
            virtualMachine.IncluirAI(ia, 15, 0, 0);

            stackCase.add(ia.LC);

            intermediateCode.add(new Instruction(ia.LC, "DSVT", "-", "?"));
            virtualMachine.IncluirAI(ia, 19, 0, 0);
            break;

        case 137:

            symbolTmp = symbolTable.search(token.getName());

            if (Objects.isNull(symbolTmp) || symbolTmp.getIndex() == -1)
                throw new SemanticError(token.getName() + " não declarado.");
            else
                indexSymbolTable = symbolTmp.getIndex();

            levelVariable = symbolTable.getTable().get(indexSymbolTable).getLevel();

            break;

        case 138:

            displacementVariable = symbolTable.getTable().get(indexSymbolTable).getGeneralA();
            intermediateCode.add(new Instruction(ia.LC, "ARMZ", String.valueOf(levelVariable), String.valueOf(displacementVariable)));
            virtualMachine.IncluirAI(ia, 4, levelVariable, displacementVariable);

            break;

        case 139:

            stackFor.add(ia.LC);

            intermediateCode.add(new Instruction(ia.LC, "COPI", "-", "-"));
            virtualMachine.IncluirAI(ia, 28, levelVariable, displacementVariable);

            level = symbolTable.getTable().get(indexSymbolTable).getLevel() - currentLevel;
            displacementVariable = symbolTable.getTable().get(indexSymbolTable).getGeneralA();

            intermediateCode.add(new Instruction(ia.LC, "CRVL", String.valueOf(level), String.valueOf(displacementVariable)));
            virtualMachine.IncluirAI(ia, 2, level, displacementVariable);

            intermediateCode.add(new Instruction(ia.LC, "CMAI", "-", "-"));
            virtualMachine.IncluirAI(ia, 18, 0, 0);

            intermediateCode.add(new Instruction(ia.LC, "DSVF", "-", "?"));
            stackFor.add(ia.LC);
            virtualMachine.IncluirAI(ia, 20, 0, 0);
            stackFor.add(indexSymbolTable);

            break;

        case 140:

            indexSymbolTable = returnStack(stackFor);
            level = symbolTable.getTable().get(indexSymbolTable).getLevel() - currentLevel;
            displacementVariable = symbolTable.getTable().get(indexSymbolTable).getGeneralA();

            intermediateCode.add(new Instruction(ia.LC, "CRVL", String.valueOf(level), String.valueOf(displacementVariable)));
            virtualMachine.IncluirAI(ia, 2, level, displacementVariable);

            intermediateCode.add(new Instruction(ia.LC, "CRCT", "-", "1"));
            virtualMachine.IncluirAI(ia, 3, 0, 1);

            intermediateCode.add(new Instruction(ia.LC, "SOMA", "-", "-"));
            virtualMachine.IncluirAI(ia, 5, 0, 0);

            intermediateCode.add(new Instruction(ia.LC, "ARMZ", String.valueOf(level), String.valueOf(displacementVariable)));
            virtualMachine.IncluirAI(ia, 4, level, displacementVariable);

            stackAux = returnStack(stackFor);
            virtualMachine.AlterarAI(ia, stackAux - 1, -1, ia.LC + 1);

            stackDetour = intermediateCode.get(stackAux - 1);
            stackDetour.setOperator2(String.valueOf(ia.LC+ 1));
            intermediateCode.set(stackAux - 1, stackDetour);

            stackAux = returnStack(stackFor);
            intermediateCode.add(new Instruction(ia.LC, "DSVS", "-", String.valueOf(stackAux)));
            virtualMachine.IncluirAI(ia, 19, 0, stackAux);

            intermediateCode.add(new Instruction(ia.LC, "AMEM", "-", "-1"));
            virtualMachine.IncluirAI(ia, 24, 0, -1);

            break;

        case 141:

            intermediateCode.add(new Instruction(ia.LC, "CMIG", "-", "-"));
            virtualMachine.IncluirAI(ia, 15, 0, 0);

            break;

        case 142:

            intermediateCode.add(new Instruction(ia.LC, "CMME", "-", "-"));
            virtualMachine.IncluirAI(ia, 13, 0, 0);

            break;

        case 143:

            intermediateCode.add(new Instruction(ia.LC, "CMMA", "-", "-"));
            virtualMachine.IncluirAI(ia, 14, 0, 0);

            break;

        case 144:

            intermediateCode.add(new Instruction(ia.LC, "CMAI", "-", "-"));
            virtualMachine.IncluirAI(ia, 18, 0, 0);

            break;

        case 145:

            intermediateCode.add(new Instruction(ia.LC, "CMEI", "-", "-"));
            virtualMachine.IncluirAI(ia, 17, 0, 0);

            break;

        case 146:

            intermediateCode.add(new Instruction(ia.LC, "CMDF", "-", "-"));
            virtualMachine.IncluirAI(ia, 16, 0, 0);

            break;

        case 147:

            intermediateCode.add(new Instruction(ia.LC, "INVR", "-", "-"));
            virtualMachine.IncluirAI(ia, 9, 0, 0);

            break;

        case 148:

            intermediateCode.add(new Instruction(ia.LC, "SOMA", "-", "-"));
            virtualMachine.IncluirAI(ia, 5, 0, 0);

            break;

        case 149:

            intermediateCode.add(new Instruction(ia.LC, "SUBT", "-", "-"));
            virtualMachine.IncluirAI(ia, 6, 0, 0);

            break;

        case 150:

            intermediateCode.add(new Instruction(ia.LC, "DISJ", "-", "-"));
            virtualMachine.IncluirAI(ia, 12, 0, 0);

            break;

        case 151:

            intermediateCode.add(new Instruction(ia.LC, "MULT", "-", "-"));
            virtualMachine.IncluirAI(ia, 7, 0, 0);

            break;

        case 152:

            intermediateCode.add(new Instruction(ia.LC, "DIV", "-", "-"));
            virtualMachine.IncluirAI(ia, 8, 0, 0);

            break;

        case 153:

            intermediateCode.add(new Instruction(ia.LC, "CONJ", "-", "-"));
            virtualMachine.IncluirAI(ia, 11, 0, 0);

            break;

        case 154:

            intermediateCode.add(new Instruction(ia.LC, "CRCT", "-", token.getName()));
            virtualMachine.IncluirAI(ia, 3, 0, token.getValue());

            break;

        case 155:

            intermediateCode.add(new Instruction(ia.LC, "NEGA", "-", "-"));
            virtualMachine.IncluirAI(ia, 3, 0, 0);

            break;

        case 156:

            context = "EXPRESSAO";

            break;

        }
    }

    private void removeSymbol() {

        for (Symbol s : stackSymbolAux) {

            if (s.getLevel() == currentLevel)
                symbolTable.delete(s);
            
        }
    }

    private String getMsgError104(Token token) {
        StringBuilder sb = new StringBuilder();
        sb.append("Variável ").append(token.getName()).append(" do nível ").append(currentLevel).append(" ja existe!");

        return sb.toString();
    }

    private boolean isValidSymbol(Symbol s) {

        if (Objects.nonNull(s) && s.getIndex() != -1) {

            indexSymbolTable = s.getIndex();
            return true;

        } else {
            return false;
        }
    }

    private void initializeStack() {

        stackIf = new ArrayList<Integer>();
        stackWhile = new ArrayList<Integer>();
        stackRepeat = new ArrayList<Integer>();
        stackParameters = new ArrayList<Integer>();
        stackProcedure = new ArrayList<Integer>();
        stackCase = new ArrayList<Integer>();
        stackFor = new ArrayList<Integer>();
        stackSymbol = new ArrayList<Symbol>();
        stackSymbolAux = new ArrayList<Symbol>();
        literalArea = new HashMap<Integer, String>();
    }

    private void initializeVariables() {

        currentLevel = 0;
        nextFreePosition = 1;

        // TODO
        // Questionar o escopo[0] = 1;

        variables = 0;
        displacement = 3;
        ia.LC = 1;
        indexLiteralArea = 1;

        levelVariable = currentLevel;
        displacementVariable = displacement;

        isProcedure = false;
    }

    public String getIntermediateCode() {

        StringBuilder tmp = new StringBuilder();
        tmp.append("------------------------------------------------------\n");
        tmp.append("CÓDIGO INTERMEDIÁRIO\n");
        tmp.append("#\t" + "INST.\t" + "ARG.1\t" + "ARG.2\t\n");
        tmp.append("------------------------------------------------------\n");

        for (int i = 0; i < intermediateCode.size(); i++) {

            tmp.append(i + 1 + "\t"); 
            tmp.append(intermediateCode.get(i).getName() + "\t");
            tmp.append(intermediateCode.get(i).getOperator1() + "\t");
            tmp.append(intermediateCode.get(i).getOperator2() + "\t\n");
        }

        if (Objects.nonNull(literalArea) && !literalArea.isEmpty()) {

            tmp.append("------------------------------------------------------\n");
            tmp.append("ÁREA DE LITERAIS \n");
            tmp.append("#\t" + "LITERAL\n");

            for (int i = 0; i < literalArea.size(); i++) {
                tmp.append(i + "\t" + literalArea.get(i + 1) + "\n");
            }
        }

        return tmp.toString();
    }

    private Integer returnStack(ArrayList<Integer> stack) {

        Integer tmp = stack.get(stack.size() - 1);
        stack.remove(stack.size() - 1);

        return tmp;
    }

    private Symbol returnSymbol(ArrayList<Symbol> stack) {

        Symbol tmp = stack.get(stack.size() - 1);
        stack.remove(stack.size() - 1);

        return tmp;
    }

}