package controller;


import java.util.LinkedList;

/**
 * MOTOR DA APLICACAO. REALIZA A IDENTIFICACAO E CLASSIFICACAO DE TODOS OS TOKENS INFORMADOS
 */
public class LexicalAnalyzer {


	// INICIALIZA O ALFABETO
	private char[] letters = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	private char[] numbers = new char[]{'0','1','2','3','4','5','6','7','8','9'};

	private int index;
	private String code;
	private char current;
	private int state;
	public static  LinkedList<Token> reserved_words;
	private boolean open_comment, open_literal;

	public Token nextToken() throws LexicalError{

		StringBuffer buffer = new StringBuffer();

		// PARA CADA TOKEN INFORMADO, REALIZA A CLASSIFICACAO
		for(int i = index; i< code.length(); i++){
			index = i;
			current = code.charAt(i);		

			switch(state){
			case 0:				
				if(belong(current, letters)){					
					buffer.append(current);				
					state = 1;
				}else if(belong(current, numbers)){					
					buffer.append(current);
					state = 2;
                }else if(current == '"'){    
                    open_literal = true;
                    state =3;
				}else if(current == '+'){
					index = i+1;					
					return new Token(current+"", 2, "Sinal de adicao");
				}else if(current == '-'){
					buffer.append(current);
					state = 10;
				}else if(current == '*'){
					index = i+1;
					return new Token(current+"",4, "Sinal de multiplicacao");
				}else if(current == '/'){
					index = i+1;
					return new Token(current+"", 5, "Sinal de divisao");
				}else if(current == '<'){
					buffer.append(current);
					state = 4;
				}else if(current == '>'){
					buffer.append(current);
					state = 5;
				}else if(current == '='){
					index = i+1;
					return new Token(current+"", 10, "Sinal de Igual");
				}else if(current == ':'){
					buffer.append(current);
					state = 6;
				}else if(current == '.'){
					buffer.append(current);
					state = 7;
				}else if(current == '('){
					buffer.append(current);
					state = 8;
				}else if(current == ')'){
					index = i+1;
					return new Token(current+"", 7, "Fecha parenteses");
				}else if (current == ','){
					index = i+1;
					return new Token(current+"", 16, "Virgula");
				}else if(current == ';'){
					index = i+1;
					return new Token(current+"", 17, "Ponto e virgula");
				}else if(current == ' ' || current == '\n' || current == '\t'|| current == '\b' || current == '\f'|| current == '\r'){
					index = i+1;
				}else if(current == '$' && i == code.length() -1){
					return null;
				} else {
					throw new LexicalError("Caractere Invalido",i,1);
				}

				break;			
			// CASE 1 -> VERIFICA IDENTIFICADOR
			case 1: 
				if(belong(current, letters)|| belong(current, numbers)){
					buffer.append(current);				
				}else{
					state = 0;
					index = i;
					Token t =  new Token(buffer.toString(), 19,"Identificador");					
					for(Token f: reserved_words){						
						if(f.getName().equals(t.getName())){								
							t = f;
							break;
						}
					}					
					return t;
				}
				break;
			// CASE 2 -> VERIFICA OS numbers
			case 2:
				if(belong(current, numbers)){
					buffer.append(current);
				}else{
					state = 0;
					index = i;
					int num=0 ;
					try{
						num = Integer.parseInt(buffer.toString());
					}catch(NumberFormatException e){
						throw new LexicalError("Erro ao converter numero",i- buffer.length(),buffer.length());
					}
					if(num>32767||num<-32767 ){
						throw new LexicalError("Numero fora da escala",i- buffer.length(),buffer.length());
					}
					return new Token(buffer.toString(), 20, "Inteiro");
				}
				break;
			// CASE 3 -> VERIFICA OS LITERAIS
			case 3:
				if(current != '"'){
					buffer.append(current);	
				}else {
					state = 0;
					index = i+1;
					open_literal = false;
					if(buffer.length() > 255) throw new LexicalError("Literal excede o tamanho permitido",i- buffer.length(),buffer.length());
					return new Token(buffer.toString(), 45,"Literal");
				}
				break;

			// CASE 4 -> ANALISA TOKENS POSTERIORES AO '<'
			case 4:
				state = 0;
				if(current == '>'){
					buffer.append(current);					
					index = i+1;					
					return new Token(buffer.toString(), 15, "Operador Diferente");
				}else if(current == '='){
					buffer.append(current);
					index = i+1;				
					return new Token(buffer.toString(), 14, "Operador Menor ou Igual");
				}		
				index = i;
				return new Token(buffer.toString(), 13, "Operador Menor");


			// CASE 5 -> ANALISA TOKENS POSTERIORES AO '>'
			case 5:
				state = 0;
				if(current == '='){
					buffer.append(current);
					index = i+1;
					return new Token(buffer.toString(), 12, "Operador Maior ou Igual");
				}else{
					index = i;
					return new Token(buffer.toString(), 11, "Operador Maior");
				}

			// CASE 6 -> ANALISA TOKENS POSTERIORES AO ':'
			case 6:
				state = 0;
				if(current == '='){
					buffer.append(current);
					index = i+1;
					return new Token(buffer.toString(), 8, "Sinal de Atribuicao");
				}else{
					index = i;
					return new Token(buffer.toString(), 9, "Dois Pontos");
				}
			// CASE 7 -> ANALISA TOKENS POSTERIORES AO '.'
			case 7:
				state = 0;
				if(current == '.') throw new LexicalError("Caracter invalido ponto ponto",i- buffer.length(),buffer.length());
				else{
					index = i;
					return new Token(buffer.toString(), 18, "Ponto");
				}
			// CASE 8 -> ANALISA INICIO DE COMENTARIOS (/)
			case 8:
				if(current == '*'){
					open_comment = true;
					buffer = new StringBuffer();
					state = 9;
				}else{
					state = 0;
					index = i;
					return new Token("(", 6,"Abre parenteses");
				}
				break;

			// CASE 9 -> ANALISA FIM DE COMENTARIOS (*)
			case 9:					
				if(current == '*' && code.charAt(i+1) == ')'){
					open_comment = false;
					state = 0;
					i+=1;
				}
				break;

			// CASE 10 -> ANALISA TOKENS DE NUMEROS NEGATIVOS
			case 10:
				if(belong(current, numbers)){
					state = 2;
					buffer.append(current);
				}else{
					state = 0;
					index = i;
					return new Token('-'+"",3,"Sinal de subtracao");
				}
			}
		}
		
		// TRATAMENTO PARA OS COMENTARIOS DO CODIGO
		if(open_comment) {
			throw new LexicalError("Comentario Aberto",true,false,index);
		}
		if(open_literal) {
				throw new LexicalError("Literal Aberto",false,true, index);
		}
		return null;
	}


		/**
		 * INICIALIZADOR DA CLASSE. RELACIONA AS PALAVRAS RESERVADAS 	
		 * @param cadeia
		 */
		public LexicalAnalyzer(String code) {
			super();
			this.code = code;
			index = 0;
			reserved_words = new LinkedList<Token>();
			addReservedWord();
		}

		/**
		 * FUNCAO UTILIZADA NA ANALISE DE TOKENS. VERIFICA SE O TOKEN PERTENCE A UM DETERMINADO GRUPO.
		 * @param a
		 * @param entradas
		 * @return
		 */
		public boolean belong(char a,char[] entradas){
			for(char aux:entradas)
				if(a == aux) return true;	
			return false;
		}


		/**
		 * INSERE AS PALAVRAS RESERVADAS
		 * @param nome
		 * @param codigo
		 */
		private void addWord(String nome, int codigo){
			reserved_words.add(new Token(nome,codigo, "Palavra Reservada"));
		}
		
		/**
		 * FUNCAO ACIONADA PELO CONSTRUTOR DA CLASSE.  
		 * CADASTRA AS PALAVRAS RESERVADAS DA LINGUAGUEM
		 */
		private void addReservedWord(){
			addWord("program", 21);
			addWord("const", 22);
			addWord("var", 23);
			addWord("procedure", 24);
			addWord("begin", 25);
			addWord("end", 26);
			addWord("integer", 27);
			addWord("call", 28);
			addWord("if", 29);
			addWord("then", 30);
			addWord("else", 31);
			addWord("while", 32);
			addWord("do", 33);
			addWord("repeat", 34);
			addWord("until", 35);
			addWord("readln", 36);
			addWord("writeln", 37);
			addWord("or", 38);
			addWord("and", 39);
			addWord("not", 40);
			addWord("for", 41);
			addWord("to", 42);
			addWord("case", 43);
			addWord("of", 44);
			addWord("literal", 45);
		}
	}