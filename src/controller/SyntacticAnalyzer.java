package controller;

import java.util.ArrayList;
import java.util.Objects;

import util.Constants;
import util.ParserConstants;

public class SyntacticAnalyzer implements Constants, ParserConstants {

	SemanticAnalyzer semantic = new SemanticAnalyzer();
	ArrayList<Integer> stack = new ArrayList<Integer>();
	ArrayList<Token> input = new ArrayList<Token>();
	ArrayList<Integer> loadValue = new ArrayList<Integer>();
	StringBuilder labelInfo = new StringBuilder();
	Integer lastTerminal;
	Token lastInput;
	Boolean error;

	public SyntacticAnalyzer(ArrayList<Token> list) {
		input = list;
		error = false;
		initializeLoadValue();
	}

	public String analize() {

		stack.add(DOLLAR);
		stack.addAll(getProductionRules(0));

		// printHeader();

		while (lastStack() != DOLLAR && !error) {

			// printArray();
			// printCompare();
			// printIntermediateCode();
			checkComment();

			if (isNotEmptyArray())
				lastInput = new Token(firstInput());

			if (lastInput.getName().equals("call"))
				semantic.setCall(input.get(1).getName());

			if (lastStack() < FIRST_NON_TERMINAL) { 

				if (loadValue.contains(nextRule())) {

					try {
						semantic.executeAction(nextRule() - FIRST_SEMANTIC_ACTION, lastInput);
					} catch (SemanticError e) {
						System.err.println(e.getMessage());
						return e.getMessage();
					}

					stack.remove(getNextRuleIndex());
				}

				if (lastStack() == firstInputCode()) {

					lastTerminal = lastStack();
					stack.remove(getLastIndex());
					input.remove(firstInput());

					// printArray();

				} else if (lastStack() == EPSILON) {
					stack.remove(getLastIndex());

				} else {
					error();
				}

			} else if (lastStack() >= FIRST_SEMANTIC_ACTION) {

				try {
					semantic.executeAction(lastStack() - FIRST_SEMANTIC_ACTION, lastInput);
				} catch (SemanticError e) {
					System.err.println(e.getMessage());
					return e.getMessage();
				}

				stack.remove(getLastIndex());
			
			} else if (lastStack() != DOLLAR) {
				unstack();
			}
			
			if (isEmptyArray() && lastStack() != DOLLAR && lastStack() < FIRST_SEMANTIC_ACTION)
				error();

		}
		
		if (!error) {
			labelInfo.append("Análise sintática concluída com sucesso. \n");
		}
		
		labelInfo.append("Fim da execução da análise sintática.\n");
		labelInfo.append(semantic.getIntermediateCode() + "\n");
		
		// printIntermediateCode();

		return labelInfo.toString();
	}
	
	

	private void error() {
		
		// TODO Rever este método

		// labelInfo.append(PARSER_ERROR[lastStack()] + "\n");
		
		//caso o erro acima aponte somente o bloco,
		//validacao abaixo mostra o erro mais especifico
		// if (lastStack() > FIRST_NON_TERMINAL) 
		// 	labelInfo.append(PARSER_ERROR[lastTerminal+1] + "\n");
//			System.err.println(PARSER_ERROR[lastTerminal+1]);

			
		System.out.println("ERRO SINTÁTICO: CHAMOU ALGUM ERRO");
		error = true;
		
	}
	
	private void unstack(){
		
		Integer x = getLineTable();
		Integer a = firstInputCode() - 1;
		
		Integer y = PARSER_TABLE[x][a];
		
		if (y == -1) {
			error();
			
		} else {
			
			stack.remove(getLastIndex());
			stack.addAll(getProductionRules(y));
			
		}
	}

	private Integer getLineTable(){
		return lastStack() - FIRST_NON_TERMINAL;
	}
	
    private ArrayList<Integer> getProductionRules(int index) {
    	
    	ArrayList<Integer> tmp = new ArrayList<Integer>();
    	
    	for (int i = PRODUCTIONS[index].length -1; i >= 0 ; i--) 
    		tmp.add(PRODUCTIONS[index][i]);
    		
    	return tmp;
    }
    
    private void printArray(){
		
    	System.out.println("Stack   > " + stack);
		System.out.println("Input   > " + getInput());
    }
    
    private void printCompare(){
    	
		System.out.println("\n~~~> X: " + lastStack());
		System.out.println("~~~> a: " + firstInputCode());

		if (Objects.nonNull(lastInput))
			System.out.println("~~~> Token atual: " + lastInput.getName());
	
		System.out.println("------------------------------------------------------");
    }
    
    private void printHeader(){
    	System.out.println("------------------------------------------------------");
    }
    
    private int getLastIndex(){
    	return stack.size() - 1;
    }
	
	private int getNextRuleIndex(){
    	return stack.size() - 2;
    }
	
    private Integer lastStack() {
    	return stack.get(stack.size() - 1);
	}
	
	private Integer nextRule() {
    	return stack.get(stack.size() - 2);
    }
    
    private Token firstInput() {
    	return input.get(0);
    }    
	
	private String getInput() {

		String tmp = "";

		for (Token t : input) {
			tmp += t.getCode() + ", ";	
		}

		return tmp;
	}

    private Integer firstInputCode() {
    	
    	if (input.isEmpty())
    		return 0;
    	else 
    	   	return input.get(0).getCode();
    }    
    
    private Boolean isEmptyArray() {
    	
    	if (input.isEmpty())
    		return true;
    	else if (stack.isEmpty())
    		return true;
    	else
    		return false;
    }
    
    private Boolean isNotEmptyArray() {
    	return !isEmptyArray();
    }
    
    private void checkComment() {

		if (lastStack() == 460) 
			stack.remove(getLastIndex());

		else if (firstInputCode() == 460) 
			input.remove(firstInput());
		
	}

	private void printIntermediateCode() {
		System.out.println(semantic.getIntermediateCode());
	}
	
	private void initializeLoadValue() {

		loadValue.add(181);
		loadValue.add(182);
		loadValue.add(183);
		loadValue.add(185);
		loadValue.add(187);
		loadValue.add(191);
		loadValue.add(193);
		loadValue.add(194);
		loadValue.add(206);
		loadValue.add(207);
		loadValue.add(214);
		loadValue.add(231);
	}
}