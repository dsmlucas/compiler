package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.border.AbstractBorder;

@SuppressWarnings("serial")
public class Layout extends AbstractBorder {
	private int line_height = 16;
	private int character_height = 8;
	private int character_width = 7;
	private Color color;
	private JViewport view_port;
	
	public Layout() {
		this.color = new Color(170, 164, 164);
	}
	
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		if (this.view_port == null) {
			searchViewport(c);
		}
		
		Point point;
		if (this.view_port != null) {
			point = this.view_port.getViewPosition();			
		} else {
			point = new Point();
		}
		
		Color oldColor = g.getColor();
		g.setColor(this.color);
		
		double r = (double) height / (double) this.line_height;
		int rows = (int) (r + 0.5);
		String str = String.valueOf(rows);
		int maxLenght = str.length();
		
		int py;
		int i = 0;
		if (point.y > 0) {
			i = point.y / this.line_height;
		}
		
		int lenght;
		int px;
		for( ; i < rows; i++) {			
			str = String.valueOf(i + 1);
			lenght = str.length();
			lenght = maxLenght - lenght;
			
			py = this.line_height * i + 14;
			px = this.character_width * lenght + 2;
			//px += point.x;
			
			g.drawString(str, px, py);
		}		
		
		int left = this.calculateLeft(height) + 7;
		//left += point.x;
		g.drawLine(left, 0, left, height);
		
        g.setColor(oldColor);
	}
	
	public Insets getBorderInsets(Component c) {
		int left = this.calculateLeft(c.getHeight()) + 10;
		return new Insets(1, left, 1, 1);
	}
	
	public Insets getBorderInsets(Component c, Insets insets) {			
		insets.top = 1;
		insets.left = this.calculateLeft(c.getHeight()) + 10;
		insets.bottom = 1;
		insets.right = 1;
		return insets;
	}
	
	protected int calculateLeft(int height) {
		double r = (double) height / (double) this.line_height;
		int rows = (int) (r + 0.5);
		String str = String.valueOf(rows);
		int lenght = str.length();

		return this.character_height * lenght;
	}
	
	protected void searchViewport(Component c) {
		Container parent = c.getParent();
		if (parent instanceof JViewport) {
			this.view_port = (JViewport) parent;
		}
	}
    
    public static JPanel getPanel() {
        JPanel panel = new JPanel(new BorderLayout());

        JTextArea area = new JTextArea();
        area.setBorder(new Layout());
        
        JScrollPane scroll = new JScrollPane(area);
        panel.add(scroll);                
        return panel;
    }
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setContentPane(getPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300);
        frame.setVisible(true);
    }    
}
