package util;

public interface Constants extends ParserConstants {
	
    int EPSILON  = 0;
    int DOLLAR   = 1;

    int t_TOKEN_2 = 2; //"+"
    int t_TOKEN_3 = 3; //"-"
    int t_TOKEN_4 = 4; //"*"
    int t_TOKEN_5 = 5; //"/"
    int t_TOKEN_6 = 6; //"("
    int t_TOKEN_7 = 7; //")"
    int t_TOKEN_8 = 8; //":="
    int t_TOKEN_9 = 9; //":"
    int t_TOKEN_10 = 10; //"="
    int t_TOKEN_11 = 11; //">"
    int t_TOKEN_12 = 12; //">="
    int t_TOKEN_13 = 13; //"<"
    int t_TOKEN_14 = 14; //"<="
    int t_TOKEN_15 = 15; //"<>"
    int t_TOKEN_16 = 16; //","
    int t_TOKEN_17 = 17; //";"
    int t_TOKEN_18 = 18; //"."
    int t_ident = 19;
    int t_inteiro = 20;
    int t_program = 21;
    int t_const = 22;
    int t_var = 23;
    int t_procedure = 24;
    int t_begin = 25;
    int t_end = 26;
    int t_integer = 27;
    int t_call = 28;
    int t_if = 29;
    int t_then = 30;
    int t_else = 31;
    int t_while = 32;
    int t_do = 33;
    int t_repeat = 34;
    int t_until = 35;
    int t_readln = 36;
    int t_writeln = 37;
    int t_or = 38;
    int t_and = 39;
    int t_not = 40;
    int t_for = 41;
    int t_to = 42;
    int t_case = 43;
    int t_of = 44;
    int t_literal = 45;
    
}
